# README #

Deep Clean Inc. 3D game's Cheese Puff Cleaning level clone

First movement class created, it uses rigidbody species of vacuum cleaners and for smooth movement normalize and quaternion rotation used.

For game play control there is a manager and it controls which vacuum prefab should be actived with instances.

For first vacuum which is hand vacuum, overlap sphere from physics used for clean cheese puffs and remove their meshrenderer.

For water vacuum same movement used but slowly and for texture cleaning setting textures pixels method used.
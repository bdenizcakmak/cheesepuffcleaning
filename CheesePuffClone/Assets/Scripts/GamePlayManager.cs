using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
   public GameObject handVacuumPrefab;
   public GameObject waterVacuumPrefab;

   public VacuumEffect instanceVac;
   private void Start()
   {
      instanceVac = VacuumEffect.Instance;
   }
   private void Update()
   {
      if (handVacuumPrefab.transform.hasChanged)
      {
         ChooseVacuum();
      }
   }
   public void ChooseVacuum()
   {
      if (instanceVac.particleCount < 0)
      {
         handVacuumPrefab.SetActive(false);
         waterVacuumPrefab.SetActive(true);
      }
   }
}

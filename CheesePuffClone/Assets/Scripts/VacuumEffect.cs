using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VacuumEffect : MonoBehaviour
{
    public static VacuumEffect Instance;
    
    public Collider[] currentCubes;
    private Collider[] _cubesInside;
    private Collider[] _cubesOutside;

    public int particleCount;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        particleCount = currentCubes.Length;
    }
    private void Update()
    {
        DisableCheesePuff();
    }
    public void DisableCheesePuff()
    {
        _cubesInside = Physics.OverlapSphere(transform.position, 0.5f); //overlap sphere method for finding colliders touching object
        foreach (var cube in _cubesInside)   //disable inside objects
        {
            var check = Array.Exists(currentCubes, x => x == cube);
            if (check)    //checking it is cheese puff, if it is, stay false
            {
                if (cube.GetComponent<SkinnedMeshRenderer>())
                {
                    cube.GetComponent<SkinnedMeshRenderer>().enabled = false;
                    particleCount--;
                }
            }
        }
    }
}

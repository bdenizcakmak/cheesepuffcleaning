using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterVacuum : MonoBehaviour
{
    public TextureDelete instanceTex;
    
    private void Start()
    {
        instanceTex = TextureDelete.Instance;
    }
    private void Update()
    {
        instanceTex.UpdateTexture();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject donePanel;

    public void StartGameButton()
    {
        startPanel.SetActive(false);
    }
}

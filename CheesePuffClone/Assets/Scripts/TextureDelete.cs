using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TextureDelete : MonoBehaviour
{
    public float radius;
    public Color InitialColor;

    private RaycastHit2D hitInfo;  //objects detected by raycasts
    
    public static TextureDelete Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public Texture2D CopyTexture2D(Texture2D copiedTexture2D)
    {
        float differenceX;
        float differenceY;

        Texture2D texture = new Texture2D(copiedTexture2D.width, copiedTexture2D.height); //copy of texture

        texture.filterMode = FilterMode.Bilinear;  //texture samples are averaged
        texture.wrapMode = TextureWrapMode.MirrorOnce;  //show animation only once
        
        int m1 = (int)((hitInfo.point.x + 2.5f) / 5 * copiedTexture2D.width);  //Center of hit point circle 
        int m2 = (int)((hitInfo.point.y + 2.5f) / 5 * copiedTexture2D.height);

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                differenceX = x - m1;
                differenceY = y - m2;

                if (differenceX * differenceX + differenceY * differenceY <= radius * radius)
                {
                    texture.SetPixel(x, y, InitialColor);  //turn all texture pixels within radius to zero alpha
                }
                else
                {
                    texture.SetPixel(x, y, copiedTexture2D.GetPixel(x, y));
                }
            }
        }
        texture.Apply(); //save texture

        return texture;
    }

    public void UpdateTexture()
    {
        SpriteRenderer mySpriteRenderer = gameObject.GetComponent<SpriteRenderer>(); //texture's sprite renderer access
        Texture2D newTexture2D = CopyTexture2D(mySpriteRenderer.sprite.texture);

        string tempName = mySpriteRenderer.sprite.name; //temporary texture
        mySpriteRenderer.sprite = Sprite.Create(newTexture2D, mySpriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f));
        mySpriteRenderer.sprite.name = tempName;
    }
}

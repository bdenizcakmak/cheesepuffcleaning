using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Movement : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    private Rigidbody rb;
    private void Awake()        
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal") * Time.deltaTime;  //x coordinate
        float verticalInput = Input.GetAxis("Vertical") * Time.deltaTime;       //y coordinate

        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        movementDirection.Normalize();  

        rb.velocity = new Vector3(horizontalInput*speed, rb.velocity.y, verticalInput*speed);      

        if(movementDirection != Vector3.zero) {
            Quaternion toRotation = Quaternion.LookRotation(movementDirection, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
    }

}
